from django.test import TestCase,Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, mhs_name
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Lab1UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_name_is_changed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>' + mhs_name + '</title>', html_response)
        self.assertIn(mhs_name , html_response)
        self.assertFalse(len(mhs_name) == 0)

    def test_mhs_name_is_written(self):
        #Content cannot be null
        self.assertIsNotNone(mhs_name)

        #Content is filled with 30 characters at least
        self.assertTrue(len(mhs_name) >= 5)

    def test_there_is_p(self):
        response = Client().get('')
        self.assertContains(response, '</p>')

    def test_there_is_button_change_themes(self):
        response = Client().get('')
        self.assertContains(response, '</button>')

    def test_there_is_a_word_Kegiatan(self):
        response = Client().get('')
        self.assertContains(response, 'Kegiatan')

    def test_there_is_a_word_Organisasi(self):
        response = Client().get('')
        self.assertContains(response, 'Organisasi')

    def test_there_is_a_word_Prestasi(self):
        response = Client().get('')
        self.assertContains(response, 'Prestasi')

class Story7TitusFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7TitusFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Story7TitusFunctionalTest, self).tearDown()

    def test_change_theme_and_click_accordion(self):
        selenium = self.selenium

        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')

        # find the element
        accordion_1 = selenium.find_element_by_id('acc1')
        accordion_2 = selenium.find_element_by_id('acc2')
        accordion_3 = selenium.find_element_by_id('acc3')
        accordion_4 = selenium.find_element_by_id('acc4')
        button_1 = selenium.find_element_by_id('btn1')
        button_2 = selenium.find_element_by_id('btn2')
        button_3 = selenium.find_element_by_id('btn3')
        button_4 = selenium.find_element_by_id('btn4')

        # test
        time.sleep(1)
        button_1.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background")
        self.assertIn('url("http://127.0.0.1:8000/static/images/bg1.jpg")',bg)

        time.sleep(1)
        button_2.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background")
        self.assertIn('url("http://127.0.0.1:8000/static/images/bg2.jpg")',bg)

        time.sleep(1)
        button_3.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background")
        self.assertIn('url("http://127.0.0.1:8000/static/images/bg3.jpg")',bg)

        time.sleep(1)
        button_4.click()
        time.sleep(1)
        bg = selenium.find_element_by_tag_name("body").value_of_css_property("background")
        self.assertIn('url("http://127.0.0.1:8000/static/images/bg4.jpg")',bg)

        accordion_1.click()
        time.sleep(1)
        accordion_1_selected = accordion_1.get_attribute("class")
        self.assertIn('active',accordion_1_selected)

        accordion_2.click()
        time.sleep(1)
        accordion_2_selected = accordion_2.get_attribute("class")
        self.assertIn('active',accordion_2_selected)

        accordion_3.click()
        time.sleep(1)
        accordion_3_selected = accordion_3.get_attribute("class")
        self.assertIn('active',accordion_3_selected)

        accordion_4.click()
        time.sleep(1)
        accordion_4_selected = accordion_4.get_attribute("class")
        self.assertIn('active',accordion_4_selected)

        accordion_4.click()
        time.sleep(1)
        accordion_4_selected = accordion_4.get_attribute("class")
        self.assertNotIn('active',accordion_4_selected)